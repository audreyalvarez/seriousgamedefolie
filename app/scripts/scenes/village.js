import Player from '@/objects/player';
import TalkBox from '@/objects/talk-box';
import TalkBoxMartine from '@/objects/talk-box-tartine';
import Pnj from '@/objects/pnj';
import Tartine from '@/objects/tartine';
import Martine from '@/objects/martine';

export default class Village extends Phaser.Scene {
  /**
   *  My custom scene.
   *
   *  @extends Phaser.Scene
   */
  constructor() {
    super({ key: 'Village' });
  }

  /**
   *  Called when this scene is initialized.
   *
   *  @protected
   *  @param {object} [data={}] - Initialization parameters.
   */
  init(/* data */) {
  }

  /**
   *  Used to declare game assets to be loaded using the loader plugin API.
   *
   *  @protected
   */
  preload() {
    this.load.json('pnj1Dialog', 'dialogues/pnj1.json');
    this.load.json('tartineDialog', 'dialogues/tartine.json');
  }

  /*
   *  Responsible for setting up game objects on the screen.
   *
   *  @protected
   *  @param {object} [data={}] - Initialization parameters.
   */
  create(/* data */) {
    let map = this.make.tilemap({
      key: 'village',
      tileWidth: 16,
      tileHeight: 16
    });

    let tiles = map.addTilesetImage('map exemple', 'tiles');
    this.pnj1Dialog = this.cache.json.get('pnj1Dialog');
    this.tartineDialog = this.cache.json.get('tartineDialog');

    this.fond = map.createDynamicLayer('fond', tiles, 0, 0);
    this.arbres = map.createDynamicLayer('arbres', tiles, 0, 0).setDepth(2);
    this.batiments = map.createDynamicLayer('batiments', tiles, 0, 0).setDepth(2);
    this.decor = map.createDynamicLayer('decor', tiles, 0, 0).setDepth(2);
    this.relief = map.createDynamicLayer('relief', tiles, 0, 0);

    this.player = new Player(this, 450, 450).setDepth(1);

    this.tartine = new Tartine(this, 490, 430).setDepth(1);
    this.pnj = new Pnj(this, 430, 430, 'pnj').setDepth(1);
    this.pnj2 = new Pnj(this, 400, 420, 'pnj2').setDepth(3);
    this.pnj3 = new Pnj(this, 476, 270, 'pnj3').setDepth(1);
    this.pnj4 = new Pnj(this, 227, 386, 'pnj4').setDepth(1);
    this.pnj5 = new Pnj(this, 673, 349, 'pnj5').setDepth(1);
    this.pnj6 = new Pnj(this, 650, 529, 'pnj6').setDepth(1);
    this.pnj7 = new Pnj(this, 316, 638, 'pnj7').setDepth(1);
    this.pnj8 = new Pnj(this, 315, 172, 'pnj8').setDepth(1);
    // console.log(this.pnj.interactionBox);
    this.arbres.setCollisionByProperty({ collide: true });
    this.batiments.setCollisionByProperty({ collide: true });
    this.decor.setCollisionByProperty({ collide: true });
    this.relief.setCollisionByProperty({ collide: true });

    this.physics.add.collider(this.player, [
      this.arbres,
      this.batiments,
      this.decor,
      this.relief,
      this.pnj,
      this.tartine,
      this.martine,
      this.pnj2,
      this.pnj3,
      this.pnj4,
      this.pnj5,
      this.pnj6,
      this.pnj7,
      this.pnj8,
    ]);
    const camera = this.cameras.main;
    camera.setZoom(2.3);
    camera.startFollow(this.player);
    camera.setBounds(0, 0, map.widthInPixels, map.heightInPixels);

    this.physics.add.overlap(this.player, this.pnj.interactionBox, (player, pnj) => {
      if (player.action) {
        this.dialogueActuel = this.pnj1Dialog.passages.premierTexte.nom;
        const dialog = new TalkBox(
          this,
          400,
          300,
          this.pnj1Dialog.nomPnj,
          this.pnj1Dialog.passages.premierTexte.texte,
          this.pnj1Dialog.passages.premierTexte.choix.map(x => x.texte)
        );
      }
    });

    this.physics.add.overlap(this.player, this.tartine.interactionBox, (player, tartine) => {
      if (player.action) {
        this.dialogueTartine = this.tartineDialog.passages.premierTexte.nom;
        const dialog = new TalkBoxMartine(
          this,
          400,
          300,
          this.tartineDialog.nomPnj,
          this.tartineDialog.passages.premierTexte.texte,
          this.tartineDialog.passages.premierTexte.choix.map(x => x.texte)
        );
      }
    });
  }

  /**
   *  Handles updates to game logic, physics and game objects.
   *
   *  @protected
   *  @param {number} t - Current internal clock time.
   *  @param {number} dt - Time elapsed since last update.
   */
  update(/* t, dt */) {
    console.log(this.player.x + " / " + this.player.y);
  }

  /**
   *  Called after a scene is rendered. Handles rendenring post processing.
   *
   *  @protected
   */
  render() {
  }

  /**
   *  Called when a scene is about to shut down.
   *
   *  @protected
   */
  shutdown() {
  }

  /**
   *  Called when a scene is about to be destroyed (i.e.: removed from scene
   *  manager). All allocated resources that need clean up should be freed up
   *  here.
   *
   *  @protected
  */
  destroy() {
  }
}
