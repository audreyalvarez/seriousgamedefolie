export default class SplashScreen extends Phaser.Scene {
  /**
   *  Takes care of loading the main game assets, including textures, tile
   *  maps, sound effects and other binary files, while displaying a busy
   *  splash screen.
   *
   *  @extends Phaser.Scene
   */
  constructor() {
    super({
      key: 'SplashScreen',

      //  Splash screen and progress bar textures.
      pack: {
        files: [{
          key: 'splash-screen',
          type: 'image'
        }, {
          key: 'progress-bar',
          type: 'image'
        }]
      }
    });
  }

  /**
   *  Show the splash screen and prepare to load game assets.
   *
   *  @protected
   */
  preload() {
    //  Display cover and progress bar textures.
    this.showCover();
    this.showProgressBar();

    //  HINT: Declare all game assets to be loaded here.
    //this.load.image('tiles', 'map/FF6MapTileCitiesTown.png');
    this.load.image('tiles', 'map/SNES - Final Fantasy 6 - Thamasa Exterior.png');
    //this.load.tilemapTiledJSON('village', 'map/map-village.json');
    this.load.tilemapTiledJSON('village', 'map/grand-village.json');

    this.load.spritesheet('jeankev', 'sprite/Free Charas/$Lanto101.png', {
      frameHeight: 32,
      frameWidth: 32,
    });

    this.load.spritesheet('martine', 'sprite/Free Charas/$Lanto (77).png', {
      frameHeight: 32,
      frameWidth: 32,
    });

    this.load.spritesheet('pnj', 'sprite/Free Charas/$Lanto105.png', {
      frameWidth: 32,
      frameHeight: 32,
    });

    this.load.spritesheet('tartine', 'sprite/Free Charas/$Lanto114.png', {
      frameWidth: 32,
      frameHeight: 32,
    });

    this.load.spritesheet('pnj2', 'sprite/Free Charas/$Lanto (96).png', {
      frameWidth: 32,
      frameHeight: 32,
    });

    this.load.spritesheet('hache', 'sprite/unicorn-icon-sprite.png', {
      frameWidth: 64,
      frameHeight: 64,
    });

    this.load.spritesheet('pnj3', 'sprite/Free Charas/$Lanto133.png', {
      frameWidth: 32,
      frameHeight: 32,
    });

    this.load.spritesheet('pnj4', 'sprite/Free Charas/$Lanto134.png', {
      frameWidth: 32,
      frameHeight: 32,
    });

    this.load.spritesheet('pnj5', 'sprite/Free Charas/$Lanto122.png', {
      frameWidth: 32,
      frameHeight: 32,
    });

    this.load.spritesheet('pnj6', 'sprite/Free Charas/$Lanto167.png', {
      frameWidth: 32,
      frameHeight: 32,
    });

    this.load.spritesheet('pnj7', 'sprite/Free Charas/$Lanto126.png', {
      frameWidth: 32,
      frameHeight: 32,
    });

    this.load.spritesheet('pnj8', 'sprite/Free Charas/$Lanto156.png', {
      frameWidth: 32,
      frameHeight: 32,
    });
  }

  /**
   *  Set up animations, plugins etc. that depend on the game assets we just
   *  loaded.
   *
   *  @protected
   */
  create() {
    //  We have nothing left to do here. Start the next scene.
    this.scene.start('Village');
    this.anims.create({
      key: 'jeankev-down',
      frames: this.anims.generateFrameNumbers('jeankev', { start: 0, end: 2 }),
      frameRate: 8,
      frameWidth: 8,
      yoyo: true,
      repeat: -1
    });
    this.anims.create({
      key: 'jeankev-left',
      frames: this.anims.generateFrameNumbers('jeankev', { start: 3, end: 5 }),
      frameRate: 8,
      frameWidth: 8,
      yoyo: true,
      repeat: -1
    });
    this.anims.create({
      key: 'jeankev-right',
      frames: this.anims.generateFrameNumbers('jeankev', { start: 6, end: 8 }),
      frameRate: 8,
      frameWidth: 8,
      yoyo: true,
      repeat: -1
    });
    this.anims.create({
      key: 'jeankev-up',
      frames: this.anims.generateFrameNumbers('jeankev', { start: 9, end: 11 }),
      frameRate: 8,
      frameWidth: 8,
      yoyo: true,
      repeat: -1
    });
    this.anims.create({
      key: 'jeankev-idle',
      frames: this.anims.generateFrameNumbers('jeankev', { start: 1, end: 1 }),
      frameWidth: 8,
      repeat: 0
    });
  }

  //  ------------------------------------------------------------------------

  /**
   *  Show the splash screen cover.
   *
   *  @private
   */
  showCover() {
    this.add.image(0, 0, 'splash-screen').setOrigin(0);
  }

  /**
   *  Show the progress bar and set up its animation effect.
   *
   *  @private
   */
  showProgressBar() {
    //  Get the progress bar filler texture dimensions.
    const { width: w, height: h } = this.textures.get('progress-bar').get();

    //  Place the filler over the progress bar of the splash screen.
    const img = this.add.sprite(82, 282, 'progress-bar').setOrigin(0);

    //  Crop the filler along its width, proportional to the amount of files
    //  loaded.
    this.load.on('progress', v => img.setCrop(0, 0, Math.ceil(v * w), h));
  }
}
