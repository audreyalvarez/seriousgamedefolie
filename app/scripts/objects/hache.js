export default class Hache extends Phaser.GameObjects.Sprite {
  /**
   *  My custom sprite.
   *
   *  @constructor
   *  @class Hache
   *  @extends Phaser.GameObjects.Sprite
   *  @param {Phaser.Scene} scene - The scene that owns this sprite.
   *  @param {number} x - The horizontal coordinate relative to the scene viewport.
   *  @param {number} y - The vertical coordinate relative to the scene viewport.
   */
  constructor(scene, x, y) {
    super(scene, x, y, 'hache');

    //  Add this game object to the owner scene.
    scene.children.add(this);
    this.scene.add.existing(this);
    this.scene.physics.world.enable(this);
    this.scene.add.image('hache');
  }
}
