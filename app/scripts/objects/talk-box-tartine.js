import { create } from "domain";

export default class TalkBoxTartine {
  /**
   *  My custom object.
   *
   *  @class TalkBoxTartine
   *  @constructor
   *  @param {Phaser.Scene} scene - The scene that owns this sprite.
   */
  constructor(scene, x, y, title, content, choices) {
    //  TODO: Stub.
    this.dialog = scene.rexUI.add.dialog({
      x: scene.tartine.x,
      y: scene.tartine.y-100,

      background: scene.rexUI.add.roundRectangle(0, 0, 100, 100, 10, 0x0e2f44),

      title: this.createLabel(scene, title),

      content: this.createLabel(scene, content),

      choices: choices.map(choice => this.createLabel(scene, choice)),

      space: {
        title: 5,
        content: 5,
        choice: 5,

        left: 5,
        right: 5,
        top: 5,
        bottom: 5,
      },

      expand: {
        content: false,  // Content is a pure text object
      }
    })
      .layout()
      .popUp(1000)
      .setDepth(3);

    this.print = scene.add.text(0, 0, '');
    this.dialog
      // .on('button.click', function (button, groupName, index) {
      //   this.print.text += index + ': ' + button.text + '\n';
      // }, this)
      .on('button.click', function (button, groupName, index) {
        console.log(index);
        console.log(choices[index]);
        this.dialog.destroy();
        scene.dialogueTartine = scene.tartineDialog.passages[scene.dialogueTartine].choix[index].lien;
        if (scene.dialogueTartine === "fermer") return;

        const dialog = new TalkBoxTartine(
          scene,
          400,
          300,
          scene.tartineDialog.nomPnj,
          scene.tartineDialog.passages[scene.dialogueTartine].texte,
          scene.tartineDialog.passages[scene.dialogueTartine].choix.map(x => x.texte)
        );
      }, this)
      .on('button.over', function (button, groupName, index) {
        button.getElement('background').setStrokeStyle(1, 0xffffff);
      })
      .on('button.out', function (button, groupName, index) {
        button.getElement('background').setStrokeStyle();
      });
  }

  createLabel(scene, text) {
    return scene.rexUI.add.label({
      width: 0, // Minimum width of round-rectangle
      height: 0, // Minimum height of round-rectangle

      background: scene.rexUI.add.roundRectangle(0, 0, 100, 40, 5, 0x000000),

      text: scene.add.text(0, 0, text, {
        fontSize: '12px',
        fontFamily: 'sans Serif',
      }),

      space: {
        left: 5,
        right: 5,
        top: 0,
        bottom: 0
      }
    });
  }
  
  destroy() {

  }
}
