export default class Tartine extends Phaser.GameObjects.Sprite {
  /**
   *  My custom sprite.
   *
   *  @constructor
   *  @class Tartine
   *  @extends Phaser.GameObjects.Sprite
   *  @param {Phaser.Scene} scene - The scene that owns this sprite.
   *  @param {number} x - The horizontal coordinate relative to the scene viewport.
   *  @param {number} y - The vertical coordinate relative to the scene viewport.
   */
  constructor(scene, x, y) {
    super(scene, x, y, 'tartine');

    this.scene.add.existing(this);
    this.scene.physics.world.enable(this);
    this.scene.add.image('tartine');
    this.body.moves = false;
    this.body.setSize(20, 20);
    this.body.offset.x = 7;
    this.body.offset.y = 10;

    this.interactionBox = this.scene.add.container(x, y);
    this.interactionBox.setSize(40, 40);
    this.scene.physics.world.enable(this.interactionBox);

    //  Add this game object to the owner scene.
    scene.children.add(this);
  }
}
