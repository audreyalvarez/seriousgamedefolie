import Hache from '@/objects/hache';
export default class Player extends Phaser.GameObjects.Sprite {
  /**
   *  My custom sprite.
   *
   *  @constructor
   *  @class Player
   *  @extends Phaser.GameObjects.Sprite
   *  @param {Phaser.Scene} scene - The scene that owns this sprite.
   *  @param {number} x - The horizontal coordinate relative to the scene viewport.
   *  @param {number} y - The vertical coordinate relative to the scene viewport.
   */
  constructor(scene, x, y) {
    super(scene, x, y, 'player');

    //  Add this game object to the owner scene.
    this.scene = scene;
    this.scene.add.existing(this);
    this.scene.physics.world.enable(this);
    this.body.collideWorldBounds = true;
    this.body.setSize(20, 20);
    this.body.offset.x = 7;
    this.body.offset.y = 5;
    this.keys = this.scene.input.keyboard.addKeys({
      left: Phaser.Input.Keyboard.KeyCodes['LEFT'],
      right: Phaser.Input.Keyboard.KeyCodes['RIGHT'],
      up: Phaser.Input.Keyboard.KeyCodes['UP'],
      down: Phaser.Input.Keyboard.KeyCodes['DOWN'],
      fire: Phaser.Input.Keyboard.KeyCodes['SPACE'],
      action: Phaser.Input.Keyboard.KeyCodes['C']
    });
    this.action = false;
    this.moving = false;

    scene.children.add(this);

    this.anims.play('jeankev-idle');

    this.axeLaunched = false;
  }

  fireAxe() {
    this.axeLaunched = true;
    this.hache = new Hache(this.scene, this.x, this.y);
    this.hache.setDepth(10);

    this.hache.speed = 400;
    this.hache.speedX = 0;
    this.hache.speedY = 0;

    if (this.direction === 'right') {
      this.hache.speedX = this.hache.speed;
    }
    if (this.direction === 'left') {
      this.hache.speedX = -this.hache.speed;
    }
    if (this.direction === 'down') {
      this.hache.speedY = this.hache.speed;
    }
    if (this.direction === 'up') {
      this.hache.speedY = -this.hache.speed;
    }
    this.hache.body.setVelocity(this.hache.speedX, this.hache.speedY);

    setTimeout(() => {
      this.hache.body.setVelocity(-this.hache.speedX, -this.hache.speedY);
    }, 300);

    setTimeout(() => {
      this.axeLaunched = false;
      this.hache.destroy();
    }, 600);
  }

  preUpdate(t, dt) {
    super.preUpdate(t, dt);
    const speed = 200;

    this.body.setVelocity(0);
    if (this.keys.left.isDown) {
      this.body.setVelocityX(-speed);
      this.direction = 'left';
    }
    if (this.keys.right.isDown) {
      this.body.setVelocityX(speed);
      this.direction = 'right';
    }
    if (this.keys.up.isDown) {
      this.body.setVelocityY(-speed);
      this.direction = 'up';
    }
    if (this.keys.down.isDown) {
      this.body.setVelocityY(speed);
      this.direction = 'down';
    }
    if (Phaser.Input.Keyboard.JustDown(this.keys.fire)) {
      if (!this.axeLaunched) {
        this.fireAxe();
      }
    }

    if (this.body.velocity.x > 0) {
      this.anims.play('jeankev-right', true);
    } else if (this.body.velocity.x < 0) {
      this.anims.play('jeankev-left', true);
    } else if (this.body.velocity.y > 0) {
      this.anims.play('jeankev-down', true);
    } else if (this.body.velocity.y < 0) {
      this.anims.play('jeankev-up', true);
    } else {
      this.anims.stop();
    }


    if (this.hache) {
      this.hache.rotation += 0.5;
    }

    if(Phaser.Input.Keyboard.JustDown(this.keys.action)){
      this.action = true;
    }else{
      this.action = false;
    }
  }
}
