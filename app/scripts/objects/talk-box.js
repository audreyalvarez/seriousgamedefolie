// :: Je créer un petit fichier qui est fait pour être exporter dans d'autres fichiers (qui sont pas dans objects mais dans scenes)
// ::Le but est que l'on ne copie colle pas a chaque fois tout ce code mais juste l'appeler dans un fichier pour qu'il fasse la mm chose
import { create } from "domain";

export default class TalkBox {
  /**
   *  My custom object.
   *
   *  @class TalkBox
   *  @constructor
   *  @param {Phaser.Scene} scene - The scene that owns this sprite.
   */

  //:: Je fais construit ma petite boite qui va dans scene, sur une abscisse et ordonnée défini, avec un titre de perso, du contenu (question) et des choix
  constructor(scene, x, y, title, content, choices) {
    //  TODO: Stub.
    this.dialog = scene.rexUI.add.dialog({
      x: scene.pnj.x,
      y: scene.pnj.y-100,

      background: scene.rexUI.add.roundRectangle(0, 0, 100, 100, 10, 0x0e2f44),

      title: this.createLabel(scene, title),

      content: this.createLabel(scene, content),

      choices: choices.map(choice => this.createLabel(scene, choice)),

      space: {
        title: 5,
        content: 5,
        choice: 5,

        left: 5,
        right: 5,
        top: 5,
        bottom: 5,
      },

      expand: {
        content: false,  // Content is a pure text object
      }
    })
      // ::Je fais apparaitre la petite boite
      .layout()
      .popUp(1000)
      .setDepth(3);

    this.print = scene.add.text(0, 0, '');
    this.dialog
      // :: Je lui de faire des trucs quand clique sur les boutons
      // .on('button.click', function (button, groupName, index) {
      //   this.print.text += index + ': ' + button.text + '\n';
      // }, this)
      .on('button.click', function (button, groupName, index) {
        console.log(index);
        console.log(choices[index]);
        this.dialog.destroy();
        scene.dialogueActuel = scene.pnj1Dialog.passages[scene.dialogueActuel].choix[index].lien;
        if (scene.dialogueActuel === "fermer") return;

        const dialog = new TalkBox(
          scene,
          400,
          300,
          scene.pnj1Dialog.nomPnj,
          scene.pnj1Dialog.passages[scene.dialogueActuel].texte,
          scene.pnj1Dialog.passages[scene.dialogueActuel].choix.map(x => x.texte)
        );
      }, this)
      // Permet de selectionner une réponse//
      .on('button.over', function (button, groupName, index) {
        button.getElement('background').setStrokeStyle(1, 0xffffff);
      })
      // Sert à ce qu'il est uniquement une seule selection//
      .on('button.out', function (button, groupName, index) {
        button.getElement('background').setStrokeStyle();
      })

    this.dialog
      .on('button.click', function (button, groupName, index) {
        console.log(this.dialog);
        this.dialog.destroy();
        console.log(choices[index]);
      }, this)
  }
  // ::Je la personnalise
  createLabel(scene, text) {
    return scene.rexUI.add.label({
      width: 0, // Minimum width of round-rectangle
      height: 0, // Minimum height of round-rectangle

      background: scene.rexUI.add.roundRectangle(0, 0, 100, 40, 5, 0x00000),

      text: scene.add.text(0, 0, text, {
        fontSize: '18px',
        fontFamily: 'sans Serif',
        align: 'center',
      }),

      space: {
        left: 5,
        right: 5,
        top: 0,
        bottom: 0
      }

    });

  }

  destroy() {

  }
}
